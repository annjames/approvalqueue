﻿import ClaimsService from '../services/claimsService';
import { receiveWarrantyClaimsType } from './WarrantyClaims';
const submitReviewClaimType = 'SUBMIT_REVIEW_CLAIM';
const reviewClaimSuccessType = 'REVIEW_CLAIM_SUCCESS';
const reviewClaimFailureType = 'REVIEW_CLAIM_FAILURE';

const openReviewClaimModalType = 'OPEN_REVIEW_CLAIM_MODAL';
const closeReviewClaimModalType = 'CLOSE_REVIEW_CLAIM_MODAL';

const initialState = {
    reviewClaim: {}
};

export const actionCreators = {

    submitReviewClaim: (reviewClaimData) => async (dispatch, getState) => {

        //Dispatch the new claim type being submitted action
        dispatch({ type: submitReviewClaimType });

        let reviewClaim, reviewClaimJson;

        try {
            var claimsService = new ClaimsService(getState);

            //Actually add the new claim
            reviewClaim = await claimsService.reviewClaim(reviewClaimData);

            if (reviewClaim.ok) {
                //Get the body of the response
                reviewClaimJson = await reviewClaim.json();
            } else {
                throw new Error("Failed to update claim.");
            }

        } catch (error) {
            dispatch({ type: reviewClaimFailureType, error });
            return;
        }
        //Assuming no error...
        //Dispatch the success action to add the new claim
        dispatch({ type: reviewClaimSuccessType, reviewClaimJson });

        //Dispatch the close modal
        dispatch({ type: closeReviewClaimModalType });

        /// need to update code to update the data start----

        //Take a copy of the existing claims and add the new claim onto it
        var reviewClaimsList = [...getState().claims.claims, reviewClaimJson];

        //Dispatch claims received action to update list w/ new claim
        dispatch({ type: receiveWarrantyClaimsType, claims: reviewClaimsList });

        /// need to update code to update the data end----

    },


    openReviewClaimModal: () => async (dispatch, getState) => {
        dispatch({ type: openReviewClaimModalType });
    },
    closeReviewClaimModal: () => async (dispatch, getState) => {
        dispatch({ type: closeReviewClaimModalType });
    }

};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {

        case openReviewClaimModalType:
            return {
                ...state,
                modalOpen: true
            }
        case openReviewClaimModalType:
            return {
                ...state,
                modalOpen: false
            }

        case submitReviewClaimType:
            return {
                ...state,
                submitting: true
            };
        case reviewClaimSuccessType:

            return {
                ...state,
                submitting: false
            };
        case reviewClaimFailureType:
            return {
                ...state,
                submitting: false,
                error: action.error
            };
        default:
            return {
                ...state

            };
    }
};
