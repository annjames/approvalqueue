﻿import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import Form from "react-jsonschema-form";
import PropTypes from 'prop-types';
import { actionCreators } from '../store/ReviewClaim';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class ReviewClaimModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };
        this.toggle = this.toggle.bind(this);
        this.reviewClaim = this.reviewClaim.bind(this);

        this.schema = {
            "title": "Claim Review",
            "description": "Please approve or deny the claim and specify your rationale for doing so.",
            "type": "object",
            "required": [
                "rationale"
            ],
            "properties": {
                "rationale": {
                    "type": "string",
                    "title": "Rationale"
                }
            }
        };

        this.ui = {
            "rationale": {
                "ui:widget": "textarea"
            }
        };
    }

    toggle() {
        if (this.props.modalOpen) {
            this.props.closeReviewClaimModal();
        } else {
            this.props.openReviewClaimModal();
        }

    }

    onSubmit = async (form) => {
        await this.props.submitReviewClaim(form.formData);
    }

    reviewClaim() {
        this.formRef.submit();
    }

    render() {
        if (this.props.isVisible) {
            let renderError = this.props.error ? <Alert color="danger">{this.props.error.message}</Alert> : "";
            return (
                <div>
                    <div >
                        <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button>
                        <Modal isOpen={this.props.modalOpen} toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                            <ModalBody>
                                <Form schema={this.schema} uiSchema={this.ui} ref={(form) => { this.formRef = form; }} onSubmit={this.onSubmit}>
                                    <br />
                                </Form>
                                {renderError}
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={this.approveClaim} disabled={this.props.submitting}>Approve</Button>{' '}
                                <Button color="primary" onClick={this.rejectClaim} disabled={this.props.submitting}>Reject</Button>{' '}
                                <Button color="secondary" onClick={this.toggle} disabled={this.props.submitting}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                    </div>
                </div>
            );
        }
        return (<div></div>);

    }
}

ReviewClaimModal.propTypes = {
    isVisible: PropTypes.bool,
    onSuccess: PropTypes.func,
    buttonLabel: PropTypes.string
}

export default connect(
    state => state.newClaim,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(ReviewClaimModal);
